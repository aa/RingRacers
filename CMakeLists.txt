cmake_minimum_required(VERSION 3.14 FATAL_ERROR)

if("${CMAKE_CURRENT_BINARY_DIR}" STREQUAL "${CMAKE_CURRENT_SOURCE_DIR}")
	message(FATAL_ERROR "In-source builds are blocked. Please build from a separate directory.")
endif()

# Set up CMAKE path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

include(CMakeDependentOption)
include(CheckCXXCompilerFlag)

file(STRINGS src/version.h SRB2_VERSION)
string(REGEX MATCH "[0-9]+\\.[0-9.]+" SRB2_VERSION ${SRB2_VERSION})

# DO NOT CHANGE THIS SRB2 STRING! Some variable names depend on this string.
# Version change is fine.
project(SRB2
	VERSION ${SRB2_VERSION}
	LANGUAGES C CXX)

if(APPLE)
	# DiscordRPC needs but does not properly specify ObjC
    enable_language(OBJC)
endif()

##### PACKAGE CONFIGURATION #####

set(SRB2_CPACK_GENERATOR "" CACHE STRING "Generator to use for making a package. E.g., ZIP, TGZ, DragNDrop (OSX only). Leave blank for default generator.")

if("${SRB2_CPACK_GENERATOR}" STREQUAL "")
	if("${CMAKE_SYSTEM_NAME}" MATCHES "Windows")
		set(SRB2_CPACK_GENERATOR "ZIP")
	elseif("${CMAKE_SYSTEM_NAME}" MATCHES "Linux")
		set(SRB2_CPACK_GENERATOR "TGZ")
	elseif("${CMAKE_SYSTEM_NAME}" MATCHES "Darwin")
		set(SRB2_CPACK_GENERATOR "TGZ")
	elseif("${CMAKE_SYSTEM_NAME}" MATCHES "FreeBSD")
		set(SRB2_CPACK_GENERATOR "TGZ")
	endif()
endif()

set(CPACK_GENERATOR ${SRB2_CPACK_GENERATOR})
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Dr. Robotnik's Ring Racers" CACHE STRING "Program name for display purposes")
set(CPACK_PACKAGE_VENDOR "Kart Krew" CACHE STRING "Vendor name for display purposes")
#set(CPACK_PACKAGE_DESCRIPTION_FILE )
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_VERSION_MAJOR ${SRB2_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${SRB2_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${SRB2_VERSION_PATCH})
set(CPACK_PACKAGE_INSTALL_DIRECTORY "CMake ${CMAKE_VERSION_MAJOR}.${CMAKE_VERSION_MINOR}")
SET(CPACK_OUTPUT_FILE_PREFIX package)
include(CPack)

# Options

option(
	SRB2_CONFIG_STATIC_STDLIB
	"Link static version of standard library. All dependencies must also be static"
	ON
)
option(SRB2_CONFIG_ENABLE_WEBM_MOVIES "Enable WebM recording support" ON)
option(SRB2_CONFIG_ENABLE_DISCORDRPC "Enable Discord RPC features" ON)
option(SRB2_CONFIG_HWRENDER "Enable hardware render (OpenGL) support" ON)
option(SRB2_CONFIG_STATIC_OPENGL "Enable static linking GL (do not do this)" OFF)
option(SRB2_CONFIG_ERRORMODE "Compile C code with warnings treated as errors." OFF)
option(SRB2_CONFIG_DEBUGMODE "Compile with PARANOIA, ZDEBUG, RANGECHECK and PACKETDROP defined." OFF)
option(SRB2_CONFIG_DEV_BUILD "Compile a development build." OFF)
option(SRB2_CONFIG_ALWAYS_MAKE_DEBUGLINK "Always make a debuglink .debug." OFF)
option(SRB2_CONFIG_TESTERS "Compile a build for testers." OFF)
option(SRB2_CONFIG_MOBJCONSISTANCY "Compile with MOBJCONSISTANCY defined." OFF)
option(SRB2_CONFIG_PACKETDROP "Compile with PACKETDROP defined." OFF)
option(SRB2_CONFIG_ZDEBUG "Compile with ZDEBUG defined." OFF)
option(SRB2_CONFIG_SKIP_COMPTIME "Skip regenerating comptime. To speed up iterative debug builds in IDEs." OFF)
# SRB2_CONFIG_PROFILEMODE is probably superceded by some CMake setting.
option(SRB2_CONFIG_PROFILEMODE "Compile for profiling (GCC only)." OFF)
option(SRB2_CONFIG_TRACY "Compile with Tracy profiling enabled" OFF)
option(SRB2_CONFIG_ASAN "Compile with AddressSanitizer (libasan)." OFF)
option(SWITCH "Build for the Nintendo Switch." OFF)
set(SRB2_CONFIG_ASSET_DIRECTORY "" CACHE PATH "Path to directory that contains all asset files for the installer. If set, assets will be part of installation and cpack.")

# Enable CCache
# (Set USE_CCACHE=ON to use, CCACHE_OPTIONS for options)
if("${CMAKE_HOST_SYSTEM_NAME}" STREQUAL Windows)
	option(USE_CCACHE "Enable ccache support" OFF)

	if(USE_CCACHE)
		find_program(CCACHE_TOOL_PATH ccache)
		if(CCACHE_TOOL_PATH)
			set(CMAKE_C_COMPILER_LAUNCHER ${CCACHE_TOOL_PATH} CACHE STRING "" FORCE)
			set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE_TOOL_PATH} CACHE STRING "" FORCE)
		else()
			message(WARNING "USE_CCACHE was set but ccache is not found (set CCACHE_TOOL_PATH)")
		endif()
	endif()
endif()

add_subdirectory(thirdparty)

if(SWITCH)
	set(DEVKITPRO $ENV{DEVKITPRO})
	include_directories(
        ${DEVKITPRO}/portlibs/switch/include/SDL2
        ${DEVKITPRO}/libnx/include
        ${DEVKITPRO}/portlibs/switch/include
    )
	list(APPEND CMAKE_PREFIX_PATH ${DEVKITPRO}/portlibs/switch/lib/cmake)
	set(PKG_CONFIG_EXECUTABLE ${DEVKITPRO}/portlibs/switch/bin/aarch64-none-elf-pkg-config)

	find_package(PkgConfig REQUIRED)

	pkg_search_module(ZLIB REQUIRED zlib)
	find_path(ZLIB_INCLUDE_DIR zlib.h HINTS ${ZLIB_INCLUDEDIR} ${ZLIB_INCLUDE_DIRS})
	find_library(ZLIB_LIBRARY NAMES z HINTS ${ZLIB_LIBDIR} ${ZLIB_LIBRARY_DIRS})
    add_library(ZLIB::ZLIB STATIC IMPORTED GLOBAL)
    set_target_properties(ZLIB::ZLIB PROPERTIES
            IMPORTED_LOCATION "${ZLIB_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ZLIB_INCLUDE_DIR}")

	pkg_search_module(PNG REQUIRED libpng)
	find_path(PNG_INCLUDE_DIR png.h HINTS ${PNG_INCLUDEDIR} ${PNG_INCLUDE_DIRS})
	find_library(PNG_LIBRARY NAMES png HINTS ${PNG_LIBDIR} ${PNG_LIBRARY_DIRS})
	add_library(PNG::PNG STATIC IMPORTED GLOBAL)
    set_target_properties(PNG::PNG PROPERTIES
            IMPORTED_LOCATION "${PNG_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PNG_INCLUDE_DIR}")

	# TODO USE A PRESET!!!!!!!!!!!
	add_definitions(-D__SWITCH__)
	set(SRB2_CONFIG_ENABLE_DISCORDRPC OFF)
	set(SRB2_CONFIG_ENABLE_STUN OFF)

	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DENABLE_NXLINK=1 -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -fPIE -g -O2 -ftls-model=local-exec -ffunction-sections -fdata-sections -fno-rtti")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DENABLE_NXLINK=1 -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -fPIE -g -O2 -ftls-model=local-exec -ffunction-sections -fdata-sections")
	set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fPIE --verbose -L${DEVKITPRO}/libnx/lib -L${DEVKITPRO}/portlibs/switch/lib -g -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -specs=${DEVKITPRO}/libnx/switch.specs -lnx -lm")
	set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -fPIE --verbose -L${DEVKITPRO}/libnx/lib -L${DEVKITPRO}/portlibs/switch/lib -g -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -specs=${DEVKITPRO}/libnx/switch.specs -lnx -lm")
	set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fPIE --verbose -L${DEVKITPRO}/libnx/lib -L${DEVKITPRO}/portlibs/switch/lib -g -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -specs=${DEVKITPRO}/libnx/switch.specs -lnx -lm")
	set(CMAKE_ASM_FLAGS "${CMAKE_ASM_FLAGS} -g -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft")
	set(CMAKE_POSITION_INDEPENDENT_CODE ON)
	# set(CMAKE_STATIC_LINKER_FLAGS ${CMAKE_STATIC_LINKER_FLAGS} -g -march=armv8-a+crc+crypto -mtune=cortex-a57 -mtp=soft -fPIE)
	include(switch)
else()
	find_package(ZLIB REQUIRED)
	find_package(PNG REQUIRED)
endif()


find_package(SDL2 CONFIG REQUIRED)
find_package(CURL REQUIRED)
# Use the one in thirdparty/fmt to guarantee a minimum version
#find_package(FMT CONFIG REQUIRED)

# libgme defaults to "Nuked" YM2612 emulator, which is
# very SLOW. The system library probably uses the
# default so just always build it.
#find_package(GME REQUIRED)

if (SRB2_CONFIG_ENABLE_WEBM_MOVIES)
	find_package(YUV REQUIRED)

	find_package(unofficial-libvpx CONFIG)
	if(NOT unofficial-libvpx_FOUND)
		find_package(VPX REQUIRED)
	endif()

	find_package(Ogg REQUIRED)
	find_package(Vorbis REQUIRED)
	find_package(VorbisEnc REQUIRED)
endif()

if(${PROJECT_SOURCE_DIR} MATCHES ${PROJECT_BINARY_DIR})
	message(FATAL_ERROR "In-source builds will bring you a world of pain. Please make a separate directory to invoke CMake from.")
endif()

if ((${SRB2_USE_CCACHE}) AND (${CMAKE_C_COMPILER} MATCHES "clang"))
	message(WARNING "Using clang and CCache: You may want to set environment variable CCACHE_CPP2=yes to prevent include errors during compile.")
endif()

# bitness check
set(SRB2_SYSTEM_BITS 0)
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
	message(STATUS "Target is 64-bit")
	set(SRB2_SYSTEM_BITS 64)
endif()
if(CMAKE_SIZEOF_VOID_P EQUAL 4)
	message(STATUS "Target is 32-bit")
	set(SRB2_SYSTEM_BITS 32)
endif()
if(${SRB2_SYSTEM_BITS} EQUAL 0)
	message(STATUS "Target bitness is unknown")
endif()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_PDB_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

# Set EXE names so the assets CMakeLists can refer to its target
set(SRB2_SDL2_EXE_NAME "" CACHE STRING "Override executable binary output name")
set(SRB2_SDL2_EXE_SUFFIX "" CACHE STRING "Optional executable suffix, separated by an underscore")

set(GIT_EXECUTABLE "git" CACHE FILEPATH "Path to git binary")

include_directories(${CMAKE_CURRENT_BINARY_DIR}/src)

add_subdirectory(src)
add_subdirectory(assets)

include(GitUtilities)

get_git_dir(SRB2_GIT_DIR)

if(NOT "${SRB2_GIT_DIR}" STREQUAL "" AND "${SRB2_SDL2_EXE_NAME}" STREQUAL "")
	# cause a reconfigure if the branch changes
	configure_file("${SRB2_GIT_DIR}/HEAD" HEAD COPYONLY)

	git_current_branch(SRB2_GIT_REVISION)

	if("${SRB2_GIT_REVISION}" STREQUAL "")
		# use abbreviated commit hash if on detached HEAD
		git_latest_commit(SRB2_GIT_REVISION)
	endif()

	list(APPEND EXE_NAME_PARTS "ringracers")

	if(NOT "${SRB2_GIT_REVISION}" STREQUAL "master")
		# substitute path-unsafe characters
		string(REGEX REPLACE "[\\\/\^\$]" "_" SAFE_REVISION "${SRB2_GIT_REVISION}")
		list(APPEND EXE_NAME_PARTS ${SAFE_REVISION})
	endif()

	if (SRB2_CONFIG_TESTERS)
		list(APPEND EXE_NAME_PARTS "TESTERS")
	endif()
elseif(NOT "${SRB2_SDL2_EXE_NAME}" STREQUAL "")
	list(APPEND EXE_NAME_PARTS ${SRB2_SDL2_EXE_NAME})
else()
	list(APPEND EXE_NAME_PARTS "ringracers")
endif()

list(APPEND EXE_NAME_PARTS ${SRB2_SDL2_EXE_SUFFIX})

list(JOIN EXE_NAME_PARTS "_" EXE_NAME)
set_target_properties(SRB2SDL2 PROPERTIES OUTPUT_NAME ${EXE_NAME})

if (SWITCH)
	add_nro_target(ringracers
	SRB2SDL2
	"Dr. Robotnik's Ring Racers"
	"Kart Krew and AAGaming"
	"${SAFE_REVISION}"
	"../docs/logo.png"
	"../res"
	)
endif()
